<?php
$app_path = realpath(__DIR__ . "/evernote-cloud-sdk-php-master/src");
$paths = array($app_path,get_include_path());
set_include_path(implode(PATH_SEPARATOR, $paths));
$headers = apache_request_headers();
header("Access-Control-Allow-Origin: http://localhost:63342");
header("Access-Control-Allow-Headers: x-token");

$rqMethod = $_SERVER['REQUEST_METHOD'];

if ($rqMethod == "OPTIONS")
    die();

require 'autoload.php';

$client = new \Evernote\Client($headers["x-token"], false, null, null, false);

$qry = $_SERVER["QUERY_STRING"];
if ($qry == "notebooks") {
    list_notebooks();
} else if (strpos($qry,"notebook=") === 0) {
    list_notes(substr($qry,9));
} else if (strpos($qry,"note=") === 0) {
    $noteGuid = substr($qry,5);
    if ($rqMethod == "GET")
        get_note($noteGuid);
    else if ($rqMethod == "POST")
        post_note($noteGuid);
} else {
    print($qry);
}

function list_notebooks() {
    global $client;
    $results = $client->listNotebooks();
    $ret = [];
    foreach ($results as $notebook) {
        $ret[] = array("title" => $notebook->name,"guid" => $notebook->guid);
    }
    echo json_encode($ret);
}

function list_notes($notebookGuid) {
    global $client;
    $results = $client->findNotesWithSearch(
        new \Evernote\Model\Search(""),
        $client->getNotebook($notebookGuid),
        \Evernote\Client::SEARCH_SCOPE_PERSONAL,
        \Evernote\Client::SORT_ORDER_TITLE,
        100); //max_results

    echo json_encode($results);
}

function get_note($noteGuid) {
    global $client;
    $note = $client->getNote($noteGuid);
    echo $note->getContent();
}

function post_note($noteGuid) {
    global $client;
    $note = $client->getNote($noteGuid);
    $note->content = $_POST["note"];
    try {
        $client->replaceNote($note,$note);
    } catch (EDAM\Error\EDAMUserException $edue) {
        // Something was wrong with the note data
        // See EDAMErrorCode enumeration for error code explanation
        // http://dev.evernote.com/documentation/reference/Errors.html#Enum_EDAMErrorCode
        print "EDAMUserException: " . $edue->errorCode." - ".$edue->parameter;
    } catch (EDAM\Error\EDAMNotFoundException $ednfe) {
        // Parent Notebook GUID doesn't correspond to an actual notebook
        print "EDAMNotFoundException: Invalid parent notebook GUID";
    }
}