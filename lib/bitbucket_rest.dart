library bitbucket_rest;

import "requester.dart";
import "dart:async";
import "dart:convert";

class BitbucketRest {
	Requester _rqs;
	String _auth;

	BitbucketRest(this._rqs,this._auth) {

	}
	// full name with owner
	Future<List> issues(String repo_name) async {
		String url = "https://api.bitbucket.org/2.0/repositories/"+repo_name+"/issues";
		List ls = new List();
		do {
			String res = await _rqs.get(url,headers: {"Authorization":_auth});
			Map mp = JSON.decode(res);
			ls.addAll(mp["values"]);
			if (mp.containsKey("next") == false)
				break;
			url = mp["next"];
		} while (true);
		return ls;
	}
}