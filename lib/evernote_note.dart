library evernote_note;

import "eksemel.dart";

export "eksemel.dart";

class EvernoteNote extends Eksemel {
	EvernoteNote(): super() {}
	EvernoteNote.fromString(String s):super.fromString(s) {}

	int get noteChild {
		for (int i=0;i<children.length;++i) {
			if (children[i].tag == "en-note")
				return i;
		};
		return -1;
	}

	void divRemoval(Eksemel e) {
		for (int i=0;i<e.children.length;++i) {
			Eksemel subE = e.children[i];
			if (subE.tag == "div") {
				e.promoteChildren(i);
				--i;
				continue;
			}
			divRemoval(subE);
		}

	}

}