library requester;

import "dart:async";

abstract class Requester {
	Future<String> get(String url, {Map<String, String> headers});
	Future<String> post(String url, Map<String,String> data, {Map<String, String> headers});
}