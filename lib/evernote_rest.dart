library evernote_rest;

import "requester.dart";
import "dart:async";

class EvernoteRest {
	Requester _rqs;
	String _token;
	EvernoteRest(this._rqs,this._token) {

	}
	String full_url(String s) {
		String url = "evernote.php?"+s;
//		if (window.location.href.contains("localhost"))
			url = "http://localhost/cloud-bridges/backend/"+url;
		return url;
	}
	Future<String> getNotebooks() {
		return _rqs.get(full_url("notebooks"),headers: {"x-token":_token});
	}
	Future<String> getNotebook(String id) {
		return _rqs.get(full_url("notebook="+id),headers: {"x-token":_token});
	}
	Future<String> getNote(String id) {
		return _rqs.get(full_url("note="+id),headers: {"x-token":_token});
	}
	Future setNote(String id,String txt) {
		return _rqs.post(full_url("note="+id),{"note":txt}, headers: {"x-token":_token});
	}
}