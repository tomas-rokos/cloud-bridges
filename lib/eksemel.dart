library eksemel;

import 'package:xml/xml.dart' as xml;

class Eksemel {
	// every node properties
	String tag = "";
	String text = "";
	Map<String,String> attributes = new Map();
	List<Eksemel> children = new List();
	Eksemel parent = null;
	// root is calculated property
	Eksemel get root {
		Eksemel e = this;
		while (e.parent != null)
			e = e.parent;
		return e;
	}
	Object custom;
	// just the constructor
	Eksemel() {

	}
	Eksemel.fromString(String s) {
		parse(s);
	}
	// parse S into this Eksemel object
	void parse(String s) {
		xml.XmlDocument doc = xml.parse(s);
		_parseLevel(doc);
	}
	void _parseLevel(xml.XmlNode node) {
		node.attributes.forEach((xml.XmlAttribute a) {
			attributes[a.name.toString()] = a.value.toString();
		});
		if (node.nodeType == xml.XmlNodeType.TEXT) {
			xml.XmlText elm = node;
			text = elm.text;
		} else if (node.nodeType == xml.XmlNodeType.ELEMENT) {
			xml.XmlElement elm = node;
			tag = elm.name.toString();
		} else if (node.nodeType == xml.XmlNodeType.DOCUMENT) {
			xml.XmlDocument elm = node;
			text = elm.text;
		} else if (node.nodeType == xml.XmlNodeType.DOCUMENT_TYPE) {
			xml.XmlDoctype elm = node;
			tag = "!DOCTYPE";
			text = elm.text;
		} else if (node.nodeType == xml.XmlNodeType.PROCESSING) {
			xml.XmlProcessing elm = node;
			tag = "?xml";
			text = elm.text;
		} else {
			print("UNEXPECTED NODE TYPE: "+ node.nodeType.toString());
		}
		node.children.forEach((xml.XmlNode subnode) {
			Eksemel sube = new Eksemel();
			sube.parent = this;
			children.add(sube);
			sube._parseLevel(subnode);
		});
	}
	String get innerText {
		String txt = text;
		children.forEach((Eksemel e) {
			txt += e.innerText;
		});
		return txt;
	}
	void write(StringBuffer buff) {
		if (tag.isNotEmpty) {
			buff.write("<"+tag);
		}
		attributes.forEach((String key, String val) {
			buff.write(" " + key + "=\"" + val + "\"");
		});
		if (tag.isNotEmpty) {
			buff.write(">");
		}
		if (text.isNotEmpty) {
			buff.write(text);
		}
		children.forEach((Eksemel sube) {
			sube.write(buff);
		});
		if (tag.isNotEmpty) {
			buff.write("</"+tag+">");
		}
	}
	String toString() {
		StringBuffer buff = new StringBuffer();
		write(buff);
		return buff.toString();
	}
	void renderTree(StringBuffer buff,[int indent = 0]) {
		if (indent >= 0) {
			buff.write("<br />");
			for (int i = 0;i<indent;++i) {
				buff.write("&#124; ");
			}
		}
		if (tag.isNotEmpty) {
			buff.write("&lt;"+tag);
		}
		attributes.forEach((String key, String val) {
			buff.write(" " + key + "=\"" + val + "\"");
		});
		if (tag.isNotEmpty) {
			buff.write("&gt;");
		}
		if (text.isNotEmpty) {
			String txt = text.replaceAll("\n","\\n");
			buff.write(text.length.toString()+":\""+txt+"\"");
		}
		bool singletextElement = false;
		if (children.length == 1 && children[0].tag.isEmpty) {
			children[0].renderTree(buff,indent+1);
//			children[0].renderTree(buff,-1);
//			singletextElement = true;
		} else {
			children.forEach((Eksemel sube) {
				sube.renderTree(buff,indent+1);
			});
		}
		if (tag.isNotEmpty) {
			if (singletextElement == false) {
				buff.write("<br />");
				for (int i = 0;i<indent;++i) {
					buff.write("&#124; ");
				}
			}
			buff.write("&lt;/"+tag+"&gt;");
		}
	}
	void promoteChildren(int childIdx) {
		Eksemel removed = children.removeAt(childIdx);
		children.insertAll(childIdx,removed.children);
		removed.children.forEach((Eksemel e) {
			e.parent = this;
		});
	}
}