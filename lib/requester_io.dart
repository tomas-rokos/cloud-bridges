library requester_io;

import "dart:async";
import "package:http/http.dart" as http;
import "requester.dart";

class RequesterIO implements Requester {
	Requester() {}
	Future<String> get(String url, {Map<String, String> headers}) async {
		http.Response resp = await http.get(url,headers: headers);
		return resp.body;
	}
	Future<String> post(String url, Map<String,String> data, {Map<String, String> headers}) async {
		http.Response resp = await http.post(url,headers: headers, body: data);
		return resp.body;
	}
}