library requester_html;

import "dart:async";
import "dart:html";
import "requester.dart";

class RequesterHtml implements Requester {
	RequesterHtml() {}
	Future<String> get(String url, {Map<String, String> headers}) async {
		HttpRequest rq = await HttpRequest.request(url,requestHeaders: headers);
		return rq.responseText;
	}
	Future<String> post(String url, Map<String,String> data, {Map<String, String> headers}) async {
		HttpRequest rq = await HttpRequest.postFormData(url, data, requestHeaders: headers);
		return rq.responseText;
	}
}