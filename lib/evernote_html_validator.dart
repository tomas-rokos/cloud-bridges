library evernote_html_validator;

import "dart:html";

class EvernoteHtmlValidatorUriNavigation implements UriPolicy {
	bool allowsUri(String uri) {return true;}
}

final NodeValidatorBuilder theEvernoteHtmlValidator = new NodeValidatorBuilder.common()
	..allowElement("a", attributes: ["href","style"])
	..allowElement("en-media", attributes: ["type","hash","height","width","alt","style"])
	..allowElement("en-note",attributes: ["style"])
	..allowElement("p",attributes: ["style"])
	..allowElement("div",attributes: ["style"])
	..allowElement("table",attributes: ["style"])
	..allowElement("td",attributes: ["style"])
	..allowElement("ul",attributes: ["style"])
	..allowElement("li",attributes: ["style"])
	..allowElement("span",attributes: ["style"])
	..allowElement("h1",attributes: ["style"])
	..allowElement("h3",attributes: ["style"])
	..allowNavigation(new EvernoteHtmlValidatorUriNavigation())
;